/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import database.src.Users;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.commons.EmailSessionBean;
import model.src.UserModel;

/**
 *
 * @author milinda
 */
@WebServlet(name = "Register", urlPatterns = {"/Register"})
public class Register extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            Users user = (Users) request.getSession().getAttribute("user");
            if (user != null) {
                response.sendRedirect("/Chaarika1/dashboard.jsp");
                return;
            }

            String email, password, name, address, city, country;
            email = request.getParameter("email");
            password = request.getParameter("pass");
            name = request.getParameter("name");
            address = request.getParameter("address");
            city = request.getParameter("city");
            country = request.getParameter("country");

            UserModel um = new UserModel();

            Users userOld = um.getUserbyEmail(email);
            
            if (userOld != null) {
                response.sendRedirect("/Chaarika1/login.jsp?msg=9");    // 9 - Email is already in System
                return;
            }
            
            String fname, lname;
            String[] spltname = name.split(" ");

            fname = spltname[0];
            lname = (spltname.length >= 2 ? spltname[1] : "");

            int x = um.saveUser(email, password, 0, fname, lname, address, "", city, country);

            if (x == 0) {
                EmailSessionBean emsb = new EmailSessionBean();
                emsb.sendEmail(email, "Welcome to Travel Partner", "Hi Traveller, \nWelcome to Travel Partner. Happy Travelling :D");
                response.sendRedirect("/Chaarika1/login.jsp?msg=1");    // 1 - Registration Success
            } else {
                response.sendRedirect("/Chaarika1/register.jsp?msg=0");    // 0 - Registration Error
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
        response.sendError(405);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
