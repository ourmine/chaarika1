/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.src.PlaceModel;

/**
 *
 * @author milinda
 */
public class UpdatePlace extends HttpServlet {

    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.getWriter().write("This URL does not support GET method");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {
            String pId = request.getParameter("pId");
            String pName = request.getParameter("pName");
            String pShtDesc = request.getParameter("pShtDesc");
            String pDesc = request.getParameter("pDesc");
            String pAddress = request.getParameter("pAddress");
            String pCity = request.getParameter("pCity");
            String pLati = request.getParameter("pLati");
            String pLong = request.getParameter("pLong");
            
            PlaceModel pm = new PlaceModel();
            int x = pm.updatePlace(Integer.parseInt(pId),pName, pShtDesc, pDesc, pAddress, pCity, Double.parseDouble(pLati), Double.parseDouble(pLong));
            
            if (x==0) {
                response.sendRedirect("/Chaarika1/admin/addPlace.jsp?msg=0");
            }else if (x==1) {
                response.sendRedirect("/Chaarika1/admin/addPlace.jsp?msg=1");
            }else{
                response.sendRedirect("/Chaarika1/admin/addPlace.jsp?msg=2");
            }           
            
        } catch (Exception e) {
            e.printStackTrace();
            response.sendRedirect("/Chaarika1/admin/addPlace.jsp?msg=3");
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
