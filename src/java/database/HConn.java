/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import org.hibernate.Session;

/**
 *
 * @author milinda
 */
public class HConn {

    private static Session conn;

    private HConn() {
    }

    public static Session getHconn() {
        if (conn == null) {
            conn = FactoryManager.getSessionFactory().openSession();
        }
        return conn;
    }

}
