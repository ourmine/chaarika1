/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.

Read Me : Images serama div ekakin dashboard eke wage pennanava. click kalama model eken image ekin eka open wenava.



 */
package model.src;

import database.HConn;
import database.src.Places;
import database.src.SelectedPlaces;
import database.src.Tours;
import database.src.Users;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import model.rateUpdate.rate;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author milinda
 */
public class TourModel {

    Session conn = HConn.getHconn();

    /*
        Saving and Updating codes
     */
    //Add places to default tour
    public int addSelectedPlace(int placeId, Users user) {
        int resp = 0;

        try {
            // get the Default Tour
            Tours defaultTour = getDefaultTour(user); //called getDefaultTour in same obj
            Places place = (Places) conn.get(Places.class, placeId);

            // Create Default Tour
            if (defaultTour == null) {
                defaultTour = createTour("Trip" + (new Date().getTime()), true, user);
            }

//            System.out.println(defaultTour.getTourId() + " " + defaultTour.getTourName());
            //Save Selected place under Default Tour
            Transaction t = conn.beginTransaction();

//            Transaction trans = conn.beginTransaction();
            SelectedPlaces seltpls = new SelectedPlaces();
            seltpls.setPlaces(place);
            seltpls.setTours(defaultTour);
            seltpls.setPriority(0);
            seltpls.setVisited(0);
            seltpls.setRemoved(0);
            seltpls.setFeedback("");
            seltpls.setRank(0);

            conn.save(seltpls);

            defaultTour.setTourPlaces(defaultTour.getTourPlaces() + 1);
            conn.update(defaultTour);

            t.commit();

//            respTest = "{\"responce\":0}";    //0 for success
            resp = 0;   //0 for success

        } catch (Exception e) {
            e.printStackTrace();
            resp = 1;    //1 for Exception
        } finally {
            conn.flush();
            System.gc();
        }
        return resp;
    }

    public Tours createTour(String tourName, boolean isDefault, Users user) {
//        String respText = "";
        if (tourName.equals("")) {
            tourName = "Trip" + (new Date().getTime());
        }
        Tours tour = null;
        try {
            Transaction t = conn.beginTransaction();
            tour = new Tours(user, tourName, 0, new Date(), 0, 0, 0, null);

            //Make this Tour Default
            if (isDefault) {
                tour.setIsDefault(1);
            }

            conn.save(tour);

            t.commit();
//            Integer tourId = (Integer) conn.save(tour);

            // json encoding
            /*  tour = (Tours) conn.get(Tours.class, tourId);
            String tourNames[] = {"tourId", "userId", "tourName", "tourPlaces", "createdDate", "deleted", "finished"};
            JSONObject json = new JSONObject(tour, tourNames);
            respText = json.toString();     */
        } catch (Exception e) {
            e.printStackTrace();
//            respText = "{\"responce\":1}";    //1 for Exception
            tour = null;
        } finally {
            conn.flush();
            System.gc();
        }

        return tour;
    }

    public int changeTourName(String name, Users user) {
        int resp = 0;
        try {
            Tours tour = getDefaultTour(user);
            Transaction t = conn.beginTransaction();
            tour.setTourName(name);
            conn.save(tour);
            t.commit();
        } catch (Exception e) {
            e.printStackTrace();
            resp = 1;
        } finally {
            conn.flush();
            System.gc();
        }
        return resp;
    }

    public int changePlacePriority(int[] seltPlsIds, int[] priority) {
        int resp = 0;
        try {
            for (int i = 0; i < seltPlsIds.length; i++) {
                System.out.println(seltPlsIds[i] + " - " + priority[i]);
                SelectedPlaces selPls = getSelectedPlaces(seltPlsIds[i]);
                Transaction t = conn.beginTransaction();
                selPls.setPriority(priority[i]);
                conn.save(selPls);
                t.commit();
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.flush();
            System.gc();
        }
        return resp;
    }

    /*
        Finish the Default Tour
     */
    public void setTourEnd(Tours tour) {
        try {
            Transaction t = conn.beginTransaction();
            tour.setFinished(1);
            conn.save(tour);
            t.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.flush();
            System.gc();
        }
    }

    /*
        Update Feedback of selected places
     */
    public int updateFeedback(Integer selectedPlaceId, String feedback, Integer rank) {
        int resp = 0;
        try {
            Transaction t = conn.beginTransaction();
            SelectedPlaces spl = (SelectedPlaces) conn.get(SelectedPlaces.class, selectedPlaceId);
            spl.setFeedback(feedback);
            spl.setRank(rank);
            spl.setVisited(1);
            conn.save(spl);
            t.commit();
            /*Rate Update*/
            rate rt = new rate();
            rt.calculate();
            rt.print();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.flush();
            System.gc();
        }
        return resp;
    }

    /*
        Delete selected places
     */
    public int deleteSelectedPlace(Integer seltPlsId) {
        int resp = 0;
        try {
            Transaction t = conn.beginTransaction();
            SelectedPlaces spl = (SelectedPlaces) conn.get(SelectedPlaces.class, seltPlsId);
            conn.delete(spl);
            t.commit();
        } catch (Exception e) {
        } finally {
            conn.flush();
            System.gc();
        }
        return resp;
    }

    /*
        Set Selected Place not Visited
     */
    public int setSeltPlsNotVisit(Integer seltPlsId) {
        int resp = 0;
        try {
            Transaction t = conn.beginTransaction();
            SelectedPlaces spl = (SelectedPlaces) conn.get(SelectedPlaces.class, seltPlsId);
            spl.setVisited(3);
            conn.save(spl);
            t.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.flush();
            System.gc();
        }
        return resp;
    }

    /*
        Searching Codes
     */
 /*
        get the tour and places list
     */
    public Tours getTour(Integer tourId) {
        Tours tour = null;
        try {
            if (!tourId.equals(0)) {
                tour = (Tours) conn.load(Tours.class, tourId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tour;
    }

    /*
        get the default tour and places list
     */
    public Tours getDefaultTour(Users user) {
        Tours tour = null;
        try {
            Criteria criTour = conn.createCriteria(Tours.class);
            criTour.add(Restrictions.eq("isDefault", 1));
            if (criTour.list().size() > 0) {
                tour = (Tours) criTour.list().get(0);
                if (tour.getFinished() == 1) {
                    // set tour not default
                    Transaction t = conn.beginTransaction();
                    tour.setIsDefault(0);
                    conn.save(tour);
                    t.commit();
                    conn.flush();
                    tour = createTour("", true, user);
                }
            } else {
                tour = createTour("", true, user);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tour;
    }

    /*
        get the Selected Places in tour
     */
    public SelectedPlaces getSelectedPlaces(int id) {
        SelectedPlaces seltPls = null;
        try {
            seltPls = (SelectedPlaces) conn.load(SelectedPlaces.class, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return seltPls;
    }

    /*
        get the Selected Places in tour
     */
    public List<SelectedPlaces> getSelectedPlacesList(Tours tour) {
        List<SelectedPlaces> seltList = new LinkedList<SelectedPlaces>();
        try {
            Criteria cri = conn.createCriteria(SelectedPlaces.class);
            cri.add(Restrictions.eq("tours", tour));
            cri.add(Restrictions.eq("visited", 0));     // 0 - not yet visited, 2 - visited, 3 - end | not visited
            seltList = cri.list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return seltList;
    }


    /*
        Get Finished Tours
     */
    public List getFinishedTours(Users user) {
        List tourList = new ArrayList();
        try {
            Criteria criTour = conn.createCriteria(Tours.class);
            criTour.add(Restrictions.eq("finished", 1));
            criTour.add(Restrictions.eq("isDefault", 0));
            tourList = criTour.list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tourList;
    }

    public static void main(String[] args) {
        // For Testing purposes
//        System.out.println("1");
//        Session con = HConn.getHconn();
//        Users u = (Users) con.get(Users.class, 1);
//        con.close();
//        System.out.println("2");
//        TourModel tm = new TourModel();
//        System.out.println("3");
//        String s = tm.createTour("One", true, u);
//        System.out.println("4");
//        System.out.println(s);
    }

}
