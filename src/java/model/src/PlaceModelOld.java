/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.src;

import database.FactoryManager;
import database.src.Images;
import database.src.Interestings;
import database.src.Places;
import database.src.PlacesImages;
import database.src.PlacesInterestings;
import database.src.SelectedPlaces;
import database.src.Tours;
import database.src.Users;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.fileupload.FileItem;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author milinda
 */
public class PlaceModelOld {

    // Send Paces by Interesting as JSON
    public String getPalcesByInterest(int interestId, Users user) {
        String respStr = "";
        Session conn = FactoryManager.getSessionFactory().openSession();

        try {

            Interestings interesting = (Interestings) conn.get(Interestings.class, interestId);

            Criteria criPlaceInerest = conn.createCriteria(PlacesInterestings.class);
            criPlaceInerest.add(Restrictions.eq("interestings", interesting));
            List<PlacesInterestings> placeInterestingList = criPlaceInerest.list();
            /*
            Criteria criPlace = conn.createCriteria(Places.class);
            criPlace.add(Restrictions.eq("placesInterestingses", new HashSet<PlacesInterestings>(placeInterestingList)));
            List<Places> placeList = criPlace.list();
             */

            //Adding places into ArrayList
            List<Places> placeList = new ArrayList<Places>();
            for (PlacesInterestings placesInterestings : placeInterestingList) {
                placeList.add(placesInterestings.getPlaces());
            }

            //filter placeList under user's Default Tour
//            Tours defaultTour = (Tours) conn.get(Tours.class, user.getDefaultTour());
            Tours defaultTour = new TourModel().getDefaultTour(user);
            //Get list of selected places in the Default Tour.
            if (defaultTour != null) {
                Criteria criSelectPlace = conn.createCriteria(SelectedPlaces.class);
                criSelectPlace.add(Restrictions.eq("tours", defaultTour));
                List<SelectedPlaces> selectedPlaceList = criSelectPlace.list();

                //Remove selected places in the list.
                for (SelectedPlaces selectedPlaces : selectedPlaceList) {
                    placeList.remove(selectedPlaces.getPlaces());
                }
            }

            // json encoding
            JSONObject respJson = new JSONObject();
            JSONArray jsonArray = new JSONArray();

            for (Places places : placeList) {
                System.out.println(places.getName());
                JSONObject obj = new JSONObject();
                obj.put("placeId", places.getPlaceId());
                obj.put("name", places.getName());
                obj.put("desc", places.getDescription());
                obj.put("add1", places.getAddl1());
                obj.put("add2", places.getAddl2());
                obj.put("city", places.getCity());
                obj.put("latitude", places.getLatitude());
                obj.put("longitude", places.getLongitude());
                obj.put("longitude", places.getLongitude());
                obj.put("rank", places.getLongitude());
                JSONArray jsonArray2 = new JSONArray();
                for (Object placesimagese : places.getPlacesImageses()) {
                    PlacesImages seltImg = (PlacesImages) placesimagese;
                    Images img = seltImg.getImages();
                    if (img.getDeleted().equals(0)) {
                        JSONObject obj2 = new JSONObject();
                        obj2.put("imageId", img.getImageId());
                        obj2.put("imageName", img.getImgName());
                        obj2.put("imageUrl", img.getImgUrl());
                        jsonArray2.put(obj2);
                    }
                }
                obj.put("images", jsonArray2);
                jsonArray.put(obj);
            }
            respJson.put("places", jsonArray);
            respStr = respJson.toString();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.close();
            System.gc();
        }
        return respStr;
    }

    //Add Places details into the DB
    public int savePlace(String name, String description, String addl1, String city, double latitude, double longitude) {
        int retVal = 0;
        Session conn = FactoryManager.getSessionFactory().openSession();
        try {
            Transaction trans = conn.beginTransaction();
            Places newPlace = new Places();
            newPlace.setName(name);
            newPlace.setDescription(description);
            newPlace.setAddl1(addl1);
            newPlace.setCity(city);
            newPlace.setLatitude(latitude);
            newPlace.setLongitude(longitude);
            newPlace.setRank(0.0);
            conn.save(newPlace);
            trans.commit();

        } catch (Exception e) {
            e.printStackTrace();
            retVal = 1;
        } finally {
            conn.close();
            System.gc();
        }

        return retVal;
    }

    //Add Place Image
    public int saveImage(List<Object> items, String realPath) {
        int retVal = 0;
        Session conn = FactoryManager.getSessionFactory().openSession();
        try {
            int placeId = 0;
            String imageName = "";
            String imgUrl = "";

            for (Object element : items) {
                FileItem fileitem = (FileItem) element;
                if (fileitem.isFormField()) {
                    if (fileitem.getFieldName().equals("placeId")) {      //getFieldName()  -  FIELD eke variable name(HTML) eka   
                        placeId = Integer.parseInt(fileitem.getString());          // getString()  -  FIELD eke value eka.
                    }
                    if (fileitem.getFieldName().equals("imageName")) {
                        imageName = fileitem.getString();
                    }
                } else {
                    imgUrl = "partials/assets/img/gallery/" + System.currentTimeMillis() + fileitem.getName();
//                    File savedFile = new File("http://localhost:8081/Chaarika1/" + imgpath);                   
                    File savedFile = new File(realPath + imgUrl);
//                    req.getServletContext().getRealPath("/") 
//                    System.out.println(req.getServletContext().getRealPath("/"));
                    fileitem.write(savedFile);
                }
            }
            Places place = (Places) conn.get(Places.class, placeId);
            int noOfImgs = place.getPlacesImageses().size();

            Transaction trans = conn.beginTransaction();
            //Save Images
            Images image = new Images();
            image.setImgName(imageName);
            image.setImgUrl(imgUrl);
            image.setDeleted(0);
            conn.save(image);

            //Save Place Images
            PlacesImages placeImg = new PlacesImages(image, place);
            placeImg.setOrderNo(noOfImgs + 1);
            conn.save(placeImg);

            trans.commit();

        } catch (Exception e) {
            e.printStackTrace();
            retVal = 1;
        } finally {
            conn.close();
            System.gc();
        }
        return retVal;
    }

    public int deleteImage(int imageId) {
        int retVal = 0;
        Session conn = FactoryManager.getSessionFactory().openSession();
        try {
            Transaction t = conn.beginTransaction();
            Images image = (Images) conn.get(Images.class, imageId);
            image.setDeleted(1);
            conn.update(image);
            t.commit();

        } catch (Exception e) {
            e.printStackTrace();
            retVal = 1;
        } finally {
            conn.close();
            System.gc();
        }
        return retVal;
    }

    // for Testing
    public static void main(String[] args) {
        Session con = FactoryManager.getSessionFactory().openSession();
        Users u = (Users) con.get(Users.class, 1);
        con.close();
        PlaceModelOld pm = new PlaceModelOld();
        System.out.println("One");
        String o = pm.getPalcesByInterest(1, u);
        System.out.println(o);
        System.out.println("ela");
    }

}
