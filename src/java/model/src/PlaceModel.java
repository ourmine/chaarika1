/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.src;

import database.HConn;
import database.src.Images;
import database.src.Interestings;
import database.src.Places;
import database.src.PlacesImages;
import database.src.PlacesInterestings;
import database.src.SelectedPlaces;
import database.src.Tours;
import database.src.Users;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.fileupload.FileItem;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author milinda
 */
public class PlaceModel {

    Session conn = HConn.getHconn();
    TourModel tm = new TourModel();

    //Searching Codes Starts
    public List getInterestingList() {
        List<Interestings> intList = new LinkedList<Interestings>();
//        Session conn = FactoryManager.getSessionFactory().openSession();
        try {
            Criteria criInter = conn.createCriteria(Interestings.class);
            intList = criInter.list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
//            conn.close();
            System.gc();
        }
        return intList;
    }

    Interestings getInterestingByName(String intstrng) {
        /*
            When I give the interesting area this function return a Interesting Object.
         */
        Interestings interesting = null;
//        Session conn = FactoryManager.getSessionFactory().openSession();
        try {
            Criteria criInter = conn.createCriteria(Interestings.class);
            criInter.add(Restrictions.eq("interestingArea", intstrng));
            if (criInter.list().size() > 0) {
                interesting = (Interestings) criInter.list().get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
//            conn.close();
            System.gc();
        }
        return interesting;
    }

    public List getPlacesList(String intstrng, Users user) {
        /*This return place List according to the Interesting area*/
        List<Places> placeList = new LinkedList<Places>();
        Interestings ints = getInterestingByName(intstrng);

//        Session conn = FactoryManager.getSessionFactory().openSession();
        try {
            //get the relation of places and Interesting area.
            Criteria criPlasInter = conn.createCriteria(PlacesInterestings.class);
            if (ints != null) {
                criPlasInter.add(Restrictions.eq("interestings", ints));
            }
            criPlasInter.addOrder(Order.desc("matching"));
            List<PlacesInterestings> plIntList = criPlasInter.list();

            //get the places
            plIntList.forEach((placesinterestings) -> {
                placeList.add(placesinterestings.getPlaces());
//                System.out.println(placesinterestings.getPlaces().getName());
            });

            //filter placeList under user's Default Tour
//            if (!user.getDefaultTour().equals(0)) {
            Tours defaultTour = tm.getDefaultTour(user);
            //Get list of selected places in the Default Tour.
            if (defaultTour != null) {
                Criteria criSelectPlace = conn.createCriteria(SelectedPlaces.class);
                criSelectPlace.add(Restrictions.eq("tours", defaultTour));
                List<SelectedPlaces> selectedPlaceList = criSelectPlace.list();

                //Remove selected places in the list.
                for (SelectedPlaces selectedPlaces : selectedPlaceList) {
                    placeList.remove(selectedPlaces.getPlaces());
                }
            }
//            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
//            conn.close();
            System.gc();
        }
        return placeList;
    }

    public Places getPlace(int placeId) {
        Places pls = null;
//        Session conn = FactoryManager.getSessionFactory().openSession();
        try {
            pls = (Places) conn.get(Places.class, placeId);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
//            conn.close();
            System.gc();
        }

        return pls;
    }

    /*
        List of all places for admin view
     */
    public List<Places> getPlaesListAdmin() {
        /*This return place List according to the A-Z*/
        List<Places> placeList = null;
//        Session conn = FactoryManager.getSessionFactory().openSession();
        try {
            Criteria criPlaces = conn.createCriteria(Places.class);
            criPlaces.addOrder(Order.asc("name"));
            placeList = criPlaces.list();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
//            conn.close();
            System.gc();
        }
        return placeList;
    }

    /*
        Get Place_Image under places
     */
    public PlacesImages getPlacesImage(Places place) {
        PlacesImages plsImg = null;
        try {
            Criteria picr = conn.createCriteria(PlacesImages.class);
            picr.add(Restrictions.eq("places", place));
            if (picr.list().size() > 0) {
                plsImg = (PlacesImages) picr.list().get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return plsImg;
    }

    /*
        Get Image under places
     */
    public List<PlacesImages> getPlacesImageLst(Places place) {
        List<PlacesImages> plsImgList = null;
        try {
            Criteria picr = conn.createCriteria(PlacesImages.class);
            picr.add(Restrictions.eq("places", place));
            if (picr.list().size()>0) {
                plsImgList = picr.list();
            }            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return plsImgList;
    }

    // Searching codes Ends
    // Saving codes Starts
    /*Add Places details into the DB*/
    public int savePlace(String name, String shrtDesc, String description, String addl1, String city, double latitude, double longitude) {
        int retVal = 0;
//        Session conn = FactoryManager.getSessionFactory().openSession();
        try {
            Transaction trans = conn.beginTransaction();
            Places newPlace = new Places();
            newPlace.setName(name);
            newPlace.setShrtDesc(shrtDesc);
            newPlace.setDescription(description);
            newPlace.setAddl1(addl1);
            newPlace.setAddl2("");      // Keep empty.
            newPlace.setCity(city);
            newPlace.setLatitude(latitude);
            newPlace.setLongitude(longitude);
            newPlace.setRank(0.0);      // Default value 0
            newPlace.setVisitCount(0);  // Default value 0
            conn.save(newPlace);
            trans.commit();
        } catch (Exception e) {
            e.printStackTrace();
            retVal = 1;
        } finally {
//            conn.close();
            System.gc();
        }
        return retVal;
    }

    /*Add Place Image*/
    public int saveImage(List<Object> items, String realPath) {
        int retVal = 0;
//        Session conn = FactoryManager.getSessionFactory().openSession();
        try {
            int placeId = 0;
            String imageName = "";
            String imgUrl = "";

            for (Object element : items) {
                FileItem fileitem = (FileItem) element;
                if (fileitem.isFormField()) {
                    if (fileitem.getFieldName().equals("placeId")) {      //getFieldName()  -  FIELD eke variable name(HTML) eka   
                        placeId = Integer.parseInt(fileitem.getString());          // getString()  -  FIELD eke value eka.
                    }
                    if (fileitem.getFieldName().equals("imageName")) {
                        imageName = fileitem.getString();
                    }
                } else {
                    imgUrl = "partials/assets/img/gallery/" + System.currentTimeMillis() + fileitem.getName();
//                    File savedFile = new File("http://localhost:8081/Chaarika1/" + imgpath);                   
                    File savedFile = new File(realPath + imgUrl);
//                    req.getServletContext().getRealPath("/") 
//                    System.out.println(req.getServletContext().getRealPath("/"));
                    fileitem.write(savedFile);
                }
            }
            Places place = (Places) conn.get(Places.class, placeId);
            int noOfImgs = place.getPlacesImageses().size();

            Transaction trans = conn.beginTransaction();
            //Save Images
            Images image = new Images();
            image.setImgName(imageName);
            image.setImgUrl(imgUrl);
            image.setDeleted(0);
            conn.save(image);

            //Save Place Images
            PlacesImages placeImg = new PlacesImages(image, place);
            placeImg.setOrderNo(noOfImgs + 1);
            conn.save(placeImg);

            trans.commit();

        } catch (Exception e) {
            e.printStackTrace();
            retVal = 1;
        } finally {
//            conn.close();
            System.gc();
        }
        return retVal;
    }

    /*
        Updating Starts
     */
    public int updatePlace(int pId, String name, String shrtDesc, String description, String addl1, String city, double latitude, double longitude) {
        int retVal = 0;
//        Session conn = FactoryManager.getSessionFactory().openSession();
        try {
            Transaction trans = conn.beginTransaction();
            Places newPlace = (Places) conn.get(Places.class, pId);
            newPlace.setName(name);
            newPlace.setShrtDesc(shrtDesc);
            newPlace.setDescription(description);
            newPlace.setAddl1(addl1);
            newPlace.setAddl2("");      // Keep empty.
            newPlace.setCity(city);
            newPlace.setLatitude(latitude);
            newPlace.setLongitude(longitude);
//            newPlace.setRank(0.0);      // Default value 0
//            newPlace.setVisitCount(0);  // Default value 0
            conn.update(newPlace);
            trans.commit();
        } catch (Exception e) {
            e.printStackTrace();
            retVal = 1;
        } finally {
//            conn.close();
            System.gc();
        }
        return retVal;
    }

    /*
        Updating Ends
     */
 /*Delete Image*/
    public int deleteImage(int imageId) {
        int retVal = 0;
//        Session conn = FactoryManager.getSessionFactory().openSession();
        try {
            Transaction t = conn.beginTransaction();
            Images image = (Images) conn.get(Images.class, imageId);
            image.setDeleted(1);
            conn.update(image);
            t.commit();

        } catch (Exception e) {
            e.printStackTrace();
            retVal = 1;
        } finally {
//            conn.close();
            System.gc();
        }
        return retVal;
    }

    // Saving codes Ends
    // for Testing
//    public static void main(String[] args) {
//        Session con = FactoryManager.getSessionFactory().openSession();
//        Users u = (Users) con.get(Users.class, 1);
//
//        PlaceModel pm = new PlaceModel();
//        System.out.println("One");
//        con.close();
//        System.out.println("Three");
//
//        System.out.println("ela");
//    }
}
