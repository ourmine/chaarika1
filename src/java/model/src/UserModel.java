/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.src;

import database.HConn;
import database.src.PresonalData;
import database.src.Users;
import java.util.Date;
import java.util.List;
import model.commons.EmailSessionBean;
import model.commons.RandomString;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author milinda
 */
// Need to encrypt password. Need to apply Security.
public class UserModel {

    Session conn = HConn.getHconn();

    //Save User and Personal Data.
    public int saveUser(String email, String password, int userType, String firstName, String lastName, String addl1, String addl2, String city, String country) {
        int retVal = 0;
        password = password.hashCode() + "";
        try {
            Transaction tras = conn.beginTransaction();
            Users user = new Users(email, password, userType, new Date());
            conn.save(user);
            PresonalData personalData = new PresonalData(user, firstName, lastName, addl1, addl2, city, country);
            conn.save(personalData);
            tras.commit();

        } catch (Exception e) {
            e.printStackTrace();
            retVal = 1;
        } finally {
            conn.flush();
            System.gc();
        }
        return retVal;
    }

    //Update Personal Data
    public int updateUserPersonalData(Users user, String firstName, String lastName, String addl1, String addl2, String city, String country) {
        int retVal = 0;
        try {
            PresonalData personalData = getPersonalData(user);
            Transaction tran = conn.beginTransaction();
            personalData.setFirstName(firstName);
            personalData.setLastName(lastName);
            personalData.setAddl1(addl1);
            personalData.setAddl2(addl2);
            personalData.setCity(city);
            personalData.setCountry(country);
            conn.update(personalData);
            tran.commit();

        } catch (Exception e) {
            e.printStackTrace();
            retVal = 1;
        } finally {
            conn.flush();
            System.gc();
        }
        return retVal;
    }

    /*
        Update email of the User
     */
    public int updateEmail(Users user, String email) {
        int resp = 0;
        try {
            Users newUser = getUserbyEmail(email);
            if (newUser == null) {
                Transaction t = conn.beginTransaction();
                user.setEmail(email);
                t.commit();
                conn.update(user);
            } else {
                return 2;
            }
        } catch (Exception e) {
            e.printStackTrace();
            resp = 1;
        } finally {
            conn.flush();
            System.gc();
        }
        return resp;
    }

    /*
    Searching
    
     */
    //get Personal Data from User
    public PresonalData getPersonalData(Users user) {
        PresonalData person = null;
        try {
            Criteria cri = conn.createCriteria(PresonalData.class);
            cri.add(Restrictions.eq("users", user));
            if (cri.list().size() > 0) {
                person = (PresonalData) cri.list().get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return person;
    }

    /*
        Get User by Email email for duplicates
     */
    public Users getUserbyEmail(String email) {
        Users user = null;
        try {
            Criteria c = conn.createCriteria(Users.class);
            c.add(Restrictions.eq("email", email));
            List<Users> l = c.list();
            for (Users l1 : l) {
                user = l1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return user;
    }

    /*
        Security 
    
     */
    //Change User Type
    public int changeUserType(Users user, int userType) {
        int retVal = 0;
        try {
            Transaction tran = conn.beginTransaction();
            user.setUserType(userType);
            conn.update(user);
            tran.commit();
        } catch (Exception e) {
            e.printStackTrace();
            retVal = 1;
        } finally {
            System.gc();
        }
        return retVal;
    }

    // Change Password
    public int changePassword(Users user, String password) {
        int retVal = 0;
        password = password.hashCode() + "";
        try {
            Transaction tran = conn.beginTransaction();
            user.setPassword(password);
            conn.update(user);
            tran.commit();
        } catch (Exception e) {
            e.printStackTrace();
            retVal = 1;
        } finally {
            System.gc();
        }
        return retVal;
    }

    // Reset Password of the User by email
    public int resetPassword(String email) {
        int resp = 0;
        Users u = null;
        try {
            Transaction t = conn.beginTransaction();
            Criteria cri = conn.createCriteria(Users.class);
            cri.add(Restrictions.eq("email", email));
            if (cri.list().size() > 0) {
                u = (Users) cri.list().get(0);
                String pass = RandomString.randomString(8);
                u.setPassword(pass.hashCode() + "");
                conn.save(u);
                t.commit();
                EmailSessionBean sesemail = new EmailSessionBean();
                sesemail.sendEmail(email, "Reset Password", "New Password is " + pass);
            } else {
                resp = 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resp;
    }

    /*
        Searching codes
     */
    public Users loginCheck(String email, String password) {
        //Need to add Login Log.
        password = password.hashCode() + "";
        Users user = getUserbyEmail(email);
        if (user == null) {
            return null;
        }
        if (!user.getPassword().equals(password)) {
            return null;
        }
        return user;
    }

    public static void main(String[] args) {
        //For Tessting
    }
}
