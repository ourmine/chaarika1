/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.commons.comparators;

import database.src.Places;
import java.util.Comparator;

/**
 *
 * @author milinda
 */
public class PlaceCompRank implements Comparator<Places>{

    @Override
    public int compare(Places t1, Places t2) {
        if(t1.getRank() < t2.getRank()){
            return 1;
        } else {
            return -1;
        }
    }
    
}
