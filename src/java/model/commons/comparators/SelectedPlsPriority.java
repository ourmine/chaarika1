/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.commons.comparators;

import database.src.SelectedPlaces;
import java.util.Comparator;

/**
 *
 * @author milinda
 */
public class SelectedPlsPriority implements Comparator<SelectedPlaces>{

    @Override
    public int compare(SelectedPlaces t1, SelectedPlaces t2) {
        if(t1.getPriority()> t2.getPriority()){
            return 1;
        } else {
            return -1;
        }
    }
    
}
