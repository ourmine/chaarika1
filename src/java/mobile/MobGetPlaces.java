/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mobile;

import database.src.Images;
import database.src.Places;
import database.src.PlacesImages;
import database.src.Users;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.src.PlaceModel;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author milinda
 */
@WebServlet(name = "MobGetPlaces", urlPatterns = {"/MobGetPlaces"})
public class MobGetPlaces extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            Users user = (Users) request.getSession().getAttribute("user");
            String interesting = request.getParameter("interesting");

            PlaceModel pm = new PlaceModel();
            List <Places> placeList = pm.getPlacesList(interesting, user);
            
            // json encoding
            JSONObject respJson = new JSONObject();
            JSONArray jsonArray = new JSONArray();

            for (Places places : placeList) {
                System.out.println(places.getName());
                JSONObject obj = new JSONObject();
                obj.put("placeId", places.getPlaceId());
                obj.put("name", places.getName());
                obj.put("desc", places.getDescription());
                obj.put("add1", places.getAddl1());
                obj.put("add2", places.getAddl2());
                obj.put("city", places.getCity());
                obj.put("latitude", places.getLatitude());
                obj.put("longitude", places.getLongitude());
                obj.put("longitude", places.getLongitude());
                obj.put("rank", places.getLongitude());
                JSONArray jsonArray2 = new JSONArray();
                for (Object placesimagese : places.getPlacesImageses()) {
                    PlacesImages seltImg = (PlacesImages) placesimagese;
                    Images img = seltImg.getImages();
                    if (img.getDeleted().equals(0)) {
                        JSONObject obj2 = new JSONObject();
                        obj2.put("imageId", img.getImageId());
                        obj2.put("imageName", img.getImgName());
                        obj2.put("imageUrl", img.getImgUrl());
                        jsonArray2.put(obj2);
                    }
                }
                obj.put("images", jsonArray2);
                jsonArray.put(obj);
            }
            respJson.put("places", jsonArray);
            
            out.write(respJson.toString());
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
