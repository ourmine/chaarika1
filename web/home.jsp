<%-- 
    Document   : home
    Created on : Mar 9, 2017, 10:12:08 PM
    Author     : milinda
--%>

<%@page import="database.src.Users"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
    </head>
    <body>
        <%
            Users user = (Users) session.getAttribute("user");
            if (user == null) {
                response.sendRedirect("/Chaarika1/login.jsp");
                return;
            }
        %>
        <h1>Hello World!</h1>
    </body>
</html>
