<%-- 
    Document   : register
    Created on : Aug 3, 2017, 8:33:14 PM
    Author     : milinda
--%>

<%@page import="database.src.Users"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="assets/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

        <title>Registration | Travel Partner</title>

        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />


        <!-- Bootstrap core CSS     -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <!--<link href="assets/css/bootstrap-theme.min.css" rel="stylesheet" />-->

        <!-- Animation library for notifications   -->
        <link href="assets/css/animate.min.css" rel="stylesheet"/>

        <!--  Light Bootstrap Table core CSS    -->
        <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


        <!--  CSS for Demo Purpose, don't include it in your project     -->
        <link href="assets/css/demo.css" rel="stylesheet" />


        <!--     Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

        <style>
            .fixed-bg {
                background-image: url("assets/img/bgimg1.jpg");
                min-height: 500px;
                background-attachment: fixed;
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
                backface-visibility: hidden;
                /*color: #ffffff;*/
            }
            .logo-bg{
                background-color: #cccccc;
                opacity: 0.6;
                filter: alpha(opacity=60);
            }
        </style>

    </head>
    <body class="fixed-bg">

        <%
            Users user = (Users) session.getAttribute("user");
            if (user != null) {
                response.sendRedirect("/Chaarika1/dashboard.jsp");
                return;
            }
        %>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-6">
                            <img style="margin: auto;" alt="Travel Partner" src="assets/img/logo.png" class="img-rounded" />
                        </div>
                        <div class="col-md-3">
                        </div>
                    </div>

                    <form role="form" action="/Chaarika1/Register" method="post">
                        <div class="form-group">
                            <label for="email">
                                Email address
                            </label>
                            <input type="email" class="form-control" autofocus="autofocus" required id="email" name="email" placeholder="E-Mail" />
                        </div>
                        <div class="form-group">
                            <label for="pass">
                                Password
                            </label>
                            <input type="password" pattern=".{5,}" required class="form-control" id="pass" name="pass" placeholder="Password" />
                        </div>
                        <div class="form-group">
                            <label for="name">
                                Name
                            </label>
                            <input type="text" class="form-control" required id="name" name="name" placeholder="Your Name" />
                        </div>
                        <div class="form-group">
                            <label for="address">
                                Address
                            </label>
                            <input type="text" class="form-control" required id="address" name="address" placeholder="Your Adderss" />
                        </div>
                        <div class="form-group">
                            <label for="city">
                                City
                            </label>
                            <input type="text" class="form-control" required id="city" name="city" placeholder="Your City" />
                        </div>
                        <div class="form-group">
                            <label for="country">
                                Country
                            </label>
                            <input type="text" class="form-control" required id="country" name="country" placeholder="Your Country" />
                            <lable style="color:#ff0000" > All fields are required </lable>
                        </div>

                        <input type="submit" class="btn btn-success btn-fill btn-block" value="Register"/>

                    </form>
                    <br/>
                    <div class="form-group">

                        <lable style="color:#ffffff" > Already have account... </lable>

                    </div>
                    <a href="login.jsp" class="btn btn-success btn-fill btn-block">
                        Login
                    </a>
                    <br/>
                    <br/>
                    <br/>
                </div>
                <div class="col-md-4">
                    <%
                        String msg = request.getParameter("msg");
                        if (msg != null && !msg.equals("")) {
                            switch (msg) {
                                case "0":
                    %>                                
                    <div class="alert alert-danger">                        
                        <span>Registration Failed. Check Details and try again.</span>
                    </div>                        
                    <%
                            break;
                        case "1":
                    %>                                
                    <div class="alert alert-success">                        
                        <span>Registration Success. Please Login.</span>
                    </div>                        
                    <%
                                    break;
                            }
                        }

                    %>
                </div>
            </div>
        </div>

    </body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.min.js"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Charts Plugin -->
    <!--<script src="assets/js/chartist.min.js"></script>-->

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>-->

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="assets/js/light-bootstrap-dashboard.js"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <!--<script src="assets/js/demo.js"></script>-->

    <script type="text/javascript">
        $(document).ready(function () {

            demo.initChartist();

            $.notify({
                icon: 'pe-7s-gift',
                message: "Welcome to <b>Light Bootstrap Dashboard</b> - a beautiful freebie for every web developer."

            }, {
                type: 'info',
                timer: 4000
            });

        });
    </script>

</html>
