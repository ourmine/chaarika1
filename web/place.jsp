<%-- 
    Document   : place
    Created on : Jun 24, 2017, 8:40:51 PM
    Author     : milinda
--%>

<%@page import="database.src.Users"%>
<%@page import="model.src.PlaceModel"%>
<%@page import="database.src.Places"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="../assets/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

        <title>My Profile</title>


        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />


        <!-- Bootstrap core CSS     -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

        <!-- Animation library for notifications   -->
        <link href="assets/css/animate.min.css" rel="stylesheet"/>

        <!--  Light Bootstrap Table core CSS    -->
        <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


        <!--  CSS for Demo Purpose, don't include it in your project     -->
        <link href="assets/css/demo.css" rel="stylesheet" />


        <!--     Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

        <style>
            #map {
                height: 400px;
                width: 100%;
            }
        </style>

    </head>
    <body>

        <%
            Users user = (Users) session.getAttribute("user");
            if (user == null) {
                response.sendRedirect("/Chaarika1/login.jsp");
                return;
            }

            if (request.getParameter("placeId") == null || request.getParameter("placeId").equals("")) {
                System.out.println("Place Id not found");
//                return;
            }
            int placeId = Integer.parseInt(request.getParameter("placeId"));

            Places pls = new PlaceModel().getPlace(placeId);

            if (pls == null) {
                System.out.println("Place is null");
                return;
            }

        %>



        <div class="wrapper">
            <div class="sidebar" data-color="green" data-image="assets/img/sidebar-5.jpg">

                <!--   you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple" -->

                <!-- Side Bar -->
                <div class="sidebar-wrapper">
                    <div class="logo">
                        <a href="http://www.chaarikacom" class="simple-text">
                            Travel Partner
                        </a>
                    </div>

                    <ul class="nav">
                        <li>
                            <a href="dashboard.jsp">
                                <i class="pe-7s-graph"></i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li class="active">
                            <a href="profile.jsp">
                                <i class="pe-7s-user"></i>
                                <p>My Profile</p>
                            </a>
                        </li>
                        <li>
                            <a href="addPlace.jsp">
                                <i class="pe-7s-note2"></i>
                                <p>Add Places</p>
                            </a>
                        </li>
                        <li>
                            <a href="addPlaceImg.jsp">
                                <i class="pe-7s-news-paper"></i>
                                <p>Add Images</p>
                            </a>
                        </li>
                        <li>
                            <a href="icons.html">
                                <i class="pe-7s-science"></i>
                                <p>Icons</p>
                            </a>
                        </li>
                        <li>
                            <a href="maps.html">
                                <i class="pe-7s-map-marker"></i>
                                <p>Maps</p>
                            </a>
                        </li>
                        <li>
                            <a href="notifications.html">
                                <i class="pe-7s-bell"></i>
                                <p>Notifications</p>
                            </a>
                        </li>
                        <li class="active-pro">
                            <a href="upgrade.html">
                                <i class="pe-7s-rocket"></i>
                                <p>Upgrade to PRO</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="main-panel">
                <nav class="navbar navbar-default navbar-fixed">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#">User</a>
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-left">
                                <li>
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-dashboard"></i>
                                        <p class="hidden-lg hidden-md">Dashboard</p>
                                    </a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-globe"></i>
                                        <b class="caret hidden-sm hidden-xs"></b>
                                        <span class="notification hidden-sm hidden-xs">5</span>
                                        <p class="hidden-lg hidden-md">
                                            5 Notifications
                                            <b class="caret"></b>
                                        </p>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Notification 1</a></li>
                                        <li><a href="#">Notification 2</a></li>
                                        <li><a href="#">Notification 3</a></li>
                                        <li><a href="#">Notification 4</a></li>
                                        <li><a href="#">Another notification</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="">
                                        <i class="fa fa-search"></i>
                                        <p class="hidden-lg hidden-md">Search</p>
                                    </a>
                                </li>
                            </ul>

                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a href="">
                                        <p>Account</p>
                                    </a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <p>
                                            Dropdown
                                            <b class="caret"></b>
                                        </p>

                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <p>Log out</p>
                                    </a>
                                </li>
                                <li class="separator hidden-lg hidden-md"></li>
                            </ul>
                        </div>
                    </div>
                </nav>


                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="card">
                                    <div class="header">
                                        <h2 class="title"><%=pls.getName()%></h2>
                                    </div>
                                    <div class="content">
                                        <!--<div class="row">-->
                                        <div class="col-md-12">
                                            <h4><%=pls.getDescription()%></h4>
                                        </div>
                                        <!--</div>-->

                                        <!--<div class="row">-->
                                        <div class="col-md-12">
                                            <h4>City : <%=pls.getCity()%></h4>
                                        </div>
                                        <!--</div>-->

                                        <div class="row">
                                            <div class="col-md-8">

                                                <div id="map"></div>

                                            </div>

                                            <div class="col-md-4">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Address</label>
                                                            <textarea rows="3" readonly="readonly" class="form-control" placeholder="" name="pAddress"><%=pls.getAddl1()%></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Latitude</label>
                                                            <input type="number" readonly="readonly" class="form-control" placeholder="Latitude" name="pLati" value="<%=pls.getLatitude()%>">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Longitude</label>
                                                            <input type="number" readonly="readonly" class="form-control" placeholder="Longitude" name="pLong" value="<%=pls.getLongitude()%>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <%
                                            if (user.getUserType() == 0) {
                                        %>
                                        <a href="/Chaarika1/admin/addPlace.jsp?placeId=<%=pls.getPlaceId()%>"><button type="submit" class="btn btn-info btn-fill pull-right">Update Details</button></a>
                                        <!--                                            <input type="submit" class="btn btn-info btn-fill pull-right" value="Add Place"/>-->
                                        <div class="clearfix"></div>
                                        <%
                                            }
                                        %>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                <footer class="footer">
                    <div class="container-fluid">
                        <nav class="pull-left">
                            <ul>
                                <li>
                                    <a href="#">
                                        Home
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Company
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Portfolio
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Blog
                                    </a>
                                </li>
                            </ul>
                        </nav>
                        <p class="copyright pull-right">
                            &copy; <script>document.write(new Date().getFullYear())</script> <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
                        </p>
                    </div>
                </footer>

            </div>
        </div>

        <script>
            function initMap() {
                var uluru = {lat: <%=pls.getLatitude()%>, lng: <%=pls.getLongitude()%>};
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 11,
                    center: uluru
                });
                var marker = new google.maps.Marker({
                    position: uluru,
                    map: map
                });
            }
        </script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBaBKK9V4AVKsfZ0K5Yp2n_eky4HqleJrk&callback=initMap">
        </script>


    </body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.min.js"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Charts Plugin -->
    <script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="assets/js/light-bootstrap-dashboard.js"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="assets/js/demo.js"></script>


</html>
