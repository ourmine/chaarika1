<%-- 
    Document   : index
    Created on : Mar 21, 2017, 9:54:00 AM
    Author     : milinda
--%>

<%@page import="database.src.Users"%>
<%@page import="database.FactoryManager"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <%
            Users user = (Users) session.getAttribute("user");
            if (user == null) {
                response.sendRedirect("/Chaarika1/login.jsp");
                return;
            }
        %>


        <h1> Welcome to chaarika !</h1>
        <a href="login.jsp">Login</a>
    </body>
</html>
