<%-- 
    Document   : finishTour
    Created on : Aug 20, 2017, 1:44:20 PM
    Author     : milinda
--%>

<%@page import="database.src.Images"%>
<%@page import="database.src.PlacesImages"%>
<%@page import="model.src.PlaceModel"%>
<%@page import="java.util.LinkedList"%>
<%@page import="java.util.List"%>
<%@page import="model.commons.comparators.SelectedPlsPriority"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Set"%>
<%@page import="database.src.SelectedPlaces"%>
<%@page import="database.src.Tours"%>
<%@page import="model.src.TourModel"%>
<%@page import="database.src.Users"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="assets/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

        <title>Feedback</title>


        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />


        <!-- Bootstrap core CSS     -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

        <!-- Animation library for notifications   -->
        <link href="assets/css/animate.min.css" rel="stylesheet"/>

        <!--  Light Bootstrap Table core CSS    -->
        <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


        <!--  CSS for Demo Purpose, don't include it in your project     -->
        <link href="assets/css/demo.css" rel="stylesheet" />


        <!--     Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

    </head>
    <body>

        <%
            Users user = (Users) session.getAttribute("user");
            if (user == null) {
                response.sendRedirect("/Chaarika1/login.jsp");
                return;
            }
            TourModel tm = new TourModel();
            Tours tour = tm.getDefaultTour(user);

            String endTour = request.getParameter("endTour");
            Integer endTourInt = 0;
            if (endTour != null && !endTour.equals("")) {
                endTourInt = Integer.parseInt(endTour);
            }
            if (endTourInt.equals(1)) {
                tm.setTourEnd(tour);
            }

            List<SelectedPlaces> seltPlsList = tm.getSelectedPlacesList(tour);
            if (seltPlsList.size() > 0) {
                Collections.sort(seltPlsList, new SelectedPlsPriority());
            }


        %>

        <div class="wrapper">
            <div class="sidebar" data-color="green" data-image="assets/img/sidebar-5.jpg">

                <!--   you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple" -->

                <!-- Side Bar -->
                <div class="sidebar-wrapper">
                    <div class="logo">
                        <a href="http://www.chaarikacom" class="simple-text">
                            Travel Partner
                        </a>
                    </div>

                    <ul class="nav">
                        <li>
                            <a href="dashboard.jsp">
                                <i class="pe-7s-graph"></i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li class="active">
                            <a href="profile.jsp">
                                <i class="pe-7s-user"></i>
                                <p>My Profile</p>
                            </a>
                        </li>

                        <li class="active-pro">
                            <a href="upgrade.html" target="_blank">
                                <i class="pe-7s-rocket"></i>
                                <p>Mobile Application</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="main-panel">
                <nav class="navbar navbar-default navbar-fixed">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="#">Feedback</a>
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">                                
                                <li>
                                    <a href="/Chaarika1/logout">
                                        <p>Log out</p>
                                    </a>
                                </li>
                                <li class="separator hidden-lg hidden-md"></li>
                            </ul>
                        </div>
                    </div>
                </nav>


                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="card">
                                    <div class="header">
                                        <h4 class="title"><%=tour.getTourName()%></h4>                                         
                                    </div>

                                    <div class="content" >
                                        <div class="container-fluid">
                                            <%
                                                if (tour.getFinished().equals(0)) {
                                            %>
                                            <div class="col-md-12">
                                                <p class="text-warning">
                                                    Hi Traveller. You come to the end of your Tour. Please give us a feedback about experience. Thank You :D
                                                </p>
                                                <a class="btn btn-fill btn-success pull-right" href="?endTour=1">Finish Tour</a>
                                            </div>
                                            <%
                                            } else {
                                                for (SelectedPlaces seltPlace : seltPlsList) {
                                            %>

                                            <div class="col-md-12">
                                                <form action="/Chaarika1/UpdateFeedback" method="post">
                                                    <div class="header">
                                                        <h4 class="title"><%=seltPlace.getPlaces().getName()%></h4>
                                                    </div>
                                                    <input type="hidden" name="seltID" value="<%=seltPlace.getSelectedPlacesId()%>">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="feedback">Visited</label>
                                                            <select class="form-control" name="visited">
                                                                <option value="1">Visited</option>
                                                                <option value="2">Not Visited</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="feedback">Rank</label>
                                                            <select class="form-control" name="rank">
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="feedback">Feedback</label>
                                                        <input type="text" class="form-control" name="feedback" placeholder="Your Feedback" value="">
                                                    </div>
                                                    <input type="submit" class="btn btn-fill btn-success pull-right" value="Submit Feedback"/>
                                                </form>
                                            </div>

                                            <%  break;
                                                    }
                                                }
                                            %>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card card-user">
                                    <div class="header">
                                        <h4 class="title">Trip Places </h4>
                                    </div>
                                    <hr/>
                                    <div class="content">                                        
                                        <%
                                            for (SelectedPlaces plas : seltPlsList) {
                                        %>
                                        <a class="btn btn-block" href="#modal-container-<%=plas.getPlaces().getPlaceId()%>" data-toggle="modal"><%=plas.getPlaces().getName()%></a>                                        
                                        <%
                                            }
                                        %>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <footer class="footer">
                    <div class="container-fluid">
                        <nav class="pull-left">
                            <ul>
                                <li>
                                    <a href="#">
                                        Home
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Company
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Portfolio
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Blog
                                    </a>
                                </li>
                            </ul>
                        </nav>
                        <p class="copyright pull-right">
                            &copy; <script>document.write(new Date().getFullYear())</script> <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
                        </p>
                    </div>
                </footer>

            </div>
        </div>

    </body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.min.js"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Charts Plugin -->
    <!--<script src="assets/js/chartist.min.js"></script>-->

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="assets/js/light-bootstrap-dashboard.js"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <!--<script src="assets/js/demo.js"></script>-->

</html>