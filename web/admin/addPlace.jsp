<%-- 
    Document   : addPlace
    Created on : Jun 24, 2017, 12:09:15 AM
    Author     : milinda
--%>

<%@page import="model.src.PlaceModel"%>
<%@page import="database.src.Users"%>
<%@page import="database.src.Places"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="../assets/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

        <title>My Profile</title>


        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />


        <!-- Bootstrap core CSS     -->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />

        <!-- Animation library for notifications   -->
        <link href="../assets/css/animate.min.css" rel="stylesheet"/>

        <!--  Light Bootstrap Table core CSS    -->
        <link href="../assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


        <!--  CSS for Demo Purpose, don't include it in your project     -->
        <link href="../assets/css/demo.css" rel="stylesheet" />


        <!--     Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
        <link href="../assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

    </head>
    <body>

        <%
            Users user = (Users) session.getAttribute("user");
            if (user == null || user.getUserType()!=5) {
                response.sendRedirect("/Chaarika1/login.jsp");
                return;
            }

            int placeId = 0;
            if (request.getParameter("placeId") == null || request.getParameter("placeId").equals("")) {
                System.out.println("Place Id not found");
//                return;
            } else {
                placeId = Integer.parseInt(request.getParameter("placeId"));
            }

            Places pls = null;
            if (placeId != 0) {
                pls = new PlaceModel().getPlace(placeId);
            }

            if (pls == null) {
                System.out.println("Place is null");
//                return;
            }

        %>

        <div class="wrapper">
            <div class="sidebar" data-color="green" data-image="../assets/img/sidebar-5.jpg">

                <!--   you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple" -->

                <!-- Side Bar -->
                <div class="sidebar-wrapper">
                    <div class="logo">
                        <a href="http://www.chaarikacom" class="simple-text">
                            Travel Partner
                        </a>
                    </div>

                    <ul class="nav">
                        <li>
                            <a href="dashboard.jsp">
                                <i class="pe-7s-graph"></i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li class="active">
                            <a href="profile.jsp">
                                <i class="pe-7s-user"></i>
                                <p>My Profile</p>
                            </a>
                        </li>
                        <li>
                            <a href="addPlace.jsp">
                                <i class="pe-7s-note2"></i>
                                <p>Add Places</p>
                            </a>
                        </li>
                        <li>
                            <a href="addPlaceImg.jsp">
                                <i class="pe-7s-news-paper"></i>
                                <p>Add Images</p>
                            </a>
                        </li>
                        <li>
                            <a href="icons.html">
                                <i class="pe-7s-science"></i>
                                <p>Icons</p>
                            </a>
                        </li>
                        <li>
                            <a href="maps.html">
                                <i class="pe-7s-map-marker"></i>
                                <p>Maps</p>
                            </a>
                        </li>
                        <li>
                            <a href="notifications.html">
                                <i class="pe-7s-bell"></i>
                                <p>Notifications</p>
                            </a>
                        </li>
                        <li class="active-pro">
                            <a href="upgrade.html">
                                <i class="pe-7s-rocket"></i>
                                <p>Upgrade to PRO</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="main-panel">
                <nav class="navbar navbar-default navbar-fixed">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#">User</a>
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-left">
                                <li>
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-dashboard"></i>
                                        <p class="hidden-lg hidden-md">Dashboard</p>
                                    </a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-globe"></i>
                                        <b class="caret hidden-sm hidden-xs"></b>
                                        <span class="notification hidden-sm hidden-xs">5</span>
                                        <p class="hidden-lg hidden-md">
                                            5 Notifications
                                            <b class="caret"></b>
                                        </p>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Notification 1</a></li>
                                        <li><a href="#">Notification 2</a></li>
                                        <li><a href="#">Notification 3</a></li>
                                        <li><a href="#">Notification 4</a></li>
                                        <li><a href="#">Another notification</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="">
                                        <i class="fa fa-search"></i>
                                        <p class="hidden-lg hidden-md">Search</p>
                                    </a>
                                </li>
                            </ul>

                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a href="">
                                        <p>Account</p>
                                    </a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <p>
                                            Dropdown
                                            <b class="caret"></b>
                                        </p>

                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <p>Log out</p>
                                    </a>
                                </li>
                                <li class="separator hidden-lg hidden-md"></li>
                            </ul>
                        </div>
                    </div>
                </nav>


                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="tabbable" id="tabs-1">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#panel-1" data-toggle="tab">Add Places</a>
                                        </li>
                                        <li>
                                            <a href="places.jsp">Places</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="panel-1">

                                            <%                                                if (pls == null) {
                                            %>
                                            <div class="card">
                                                <div class="header">
                                                    <h4 class="title">Add Places</h4>
                                                </div>
                                                <div class="content">
                                                    <form action="/Chaarika1/AddPlace" method="POST">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="PlaceName">Place Name</label>
                                                                    <input type="text" class="form-control" placeholder="Place Name" name="pName">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="ShortDescription">Short Description</label>
                                                                    <input type="text" class="form-control" placeholder="Short Description" name="pShtDesc">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label>Description</label>
                                                                    <textarea rows="5" class="form-control" placeholder="Detailed Description" name="pDesc"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label>Address</label>
                                                                    <input type="text" class="form-control" placeholder="Place Address" name="pAddress">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>City</label>
                                                                    <input type="text" class="form-control" placeholder="City" name="pCity">
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Latitude</label>
                                                                    <input type="number" step="any" class="form-control" placeholder="Latitude" name="pLati">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Longitude</label>
                                                                    <input type="number" step="any" class="form-control" placeholder="Longitude" name="pLong">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--<button type="submit" class="btn btn-info btn-fill pull-right">Update Profile</button>-->
                                                        <input type="submit" class="btn btn-info btn-fill pull-right" value="Add Place"/>
                                                        <div class="clearfix"></div>
                                                    </form>
                                                </div>
                                            </div>

                                            <%                                                } else {

                                            %>
                                            <div class="card">
                                                <div class="header">
                                                    <h4 class="title">Add Places</h4>
                                                </div>
                                                <div class="content">
                                                    <form action="/Chaarika1/UpdatePlace" method="POST">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="PlaceName">Place Name</label>
                                                                    <input type="text" class="form-control" placeholder="Place Name" name="pName" value="<%=pls.getName()%>">
                                                                    <input type="hidden" class="form-control" placeholder="Place ID" name="pId" value="<%=pls.getPlaceId()%>">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="ShortDescription">Short Description</label>
                                                                    <input type="text" class="form-control" placeholder="Short Description" name="pShtDesc" value="<%=pls.getShrtDesc()%>">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label>Description</label>
                                                                    <textarea rows="5" class="form-control" placeholder="Detailed Description" name="pDesc"><%=pls.getDescription()%></textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label>Address</label>
                                                                    <input type="text" class="form-control" placeholder="Place Address" name="pAddress" value="<%=pls.getAddl1()%>">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>City</label>
                                                                    <input type="text" class="form-control" placeholder="City" name="pCity" value="<%=pls.getCity()%>">
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Latitude</label>
                                                                    <input type="number" step="any" class="form-control" placeholder="Latitude" name="pLati" value="<%=pls.getLatitude()%>">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Longitude</label>
                                                                    <input type="number" step="any" class="form-control" placeholder="Longitude" name="pLong" value="<%=pls.getLongitude()%>">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--<button type="submit" class="btn btn-info btn-fill pull-right">Update Profile</button>-->
                                                        <input type="submit" class="btn btn-info btn-fill pull-right" value="Update Detals"/>
                                                        <div class="clearfix"></div>
                                                    </form>
                                                </div>
                                            </div>

                                            <%                                                }
                                            %>


                                        </div>
                                    </div>

                                    <div class="tab-content">
                                        <div class="tab-pane " id="panel-2">
                                            <p>
                                                Places
                                            </p>
                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>
                </div>


                <footer class="footer">
                    <div class="container-fluid">
                        <nav class="pull-left">
                            <ul>
                                <li>
                                    <a href="#">
                                        Home
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Company
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Portfolio
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Blog
                                    </a>
                                </li>
                            </ul>
                        </nav>
                        <p class="copyright pull-right">
                            &copy; <script>document.write(new Date().getFullYear())</script> <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
                        </p>
                    </div>
                </footer>

            </div>
        </div>


    </body>

    <!--   Core JS Files   -->
    <script src="../assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.min.js"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="../assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Charts Plugin -->
    <script src="../assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="../assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="../assets/js/light-bootstrap-dashboard.js"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="../assets/js/demo.js"></script>

</html>