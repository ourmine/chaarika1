<%-- 
    Document   : dashboard
    Created on : Mar 21, 2017, 12:50:41 AM
    Author     : milinda

need to fix image slider in model and image dosen't work in null places images
--%>

<%@page import="database.src.PlacesImages"%>
<%@page import="database.src.Images"%>
<%@page import="model.commons.comparators.PlaceCompRank"%>
<%@page import="model.commons.comparators.PlaceCompVisited"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Collection"%>
<%@page import="database.src.Users"%>
<%@page import="model.src.PlaceModel"%>
<%@page import="database.src.Interestings"%>
<%@page import="java.util.List"%>
<%@page import="database.src.Places"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="database.FactoryManager"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="assets/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

        <title>Dashboard</title>

        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />


        <!-- Bootstrap core CSS     -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <!--<link href="assets/css/bootstrap-theme.min.css" rel="stylesheet" />-->

        <!-- Animation library for notifications   -->
        <link href="assets/css/animate.min.css" rel="stylesheet"/>

        <!--  Light Bootstrap Table core CSS    -->
        <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


        <!--  CSS for Demo Purpose, don't include it in your project     -->
        <link href="assets/css/demo.css" rel="stylesheet" />


        <!--     Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

    </head>
    <body>
        <%
            Users user = (Users) session.getAttribute("user");
            if (user == null) {
                response.sendRedirect("/Chaarika1/login.jsp");
                return;
            }

            List<Places> placeList;
            placeList = (List<Places>) request.getAttribute("places");

            PlaceModel pm = new PlaceModel();

            if (placeList == null) {
                placeList = pm.getPlaesListAdmin();
            }

            String interesting = request.getParameter("interesting");
            if (interesting == null) {
                interesting = "";
            }

        %>
        <div class="wrapper">
            <div class="sidebar" data-color="green" data-image="assets/img/sidebar-5.jpg">

                <!--
            
                    Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
                    Tip 2: you can also add an image using data-image tag
            
                -->

                <div class="sidebar-wrapper">
                    <div class="logo">
                        <a href="http://www.chaarika.com" class="simple-text">
                            Travel Partner
                        </a>
                    </div>

                    <ul class="nav">
                        <li class="active">
                            <a href="dashboard.jsp">
                                <i class="pe-7s-graph"></i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li>
                            <a href="profile.jsp">
                                <i class="pe-7s-user"></i>
                                <p>My Profile</p>
                            </a>
                        </li>

                        <li class="active-pro">
                            <a href="upgrade.html" target="_blank">
                                <i class="pe-7s-rocket"></i>
                                <p>Mobile Application</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="main-panel">
                <nav class="navbar navbar-default navbar-fixed">
                    <div class="container-fluid">
                        <div class="navbar-header">                            
                            <a class="navbar-brand" href="#">Dashboard</a>
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a href="/Chaarika1/logout">
                                        <p>Log out</p>
                                    </a>
                                </li>
                                <li class="separator hidden-lg hidden-md"></li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <!--body contend start -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-md-12">
                                <form action="/Chaarika1/GetPlaces" method="post">
                                    <div class="form-group col-sm-10">
                                        <!--
                                                                        <label for="country">
                                                                            Country
                                                                        </label>-->
                                        <input list="interesting" name="interesting" autofocus="autofocus" class="form-control" placeholder="Select Your Interestings" value="<%=interesting %>" >
                                        <datalist id="interesting" >
                                            <%                                                List<Interestings> interestingList = pm.getInterestingList();
                                                for (Interestings intest : interestingList) {
                                            %>
                                            <option value="<%=intest.getInterestingArea()%>"><%=intest.getInterestingArea()%></option>

                                            <%
                                                }
                                            %>
                                        </datalist>
                                    </div>

                                    <div class="form-group col-sm-2">
                                        <input type="submit" class="btn btn-success btn-block" value="Search">
                                    </div>
                                    <!--                                <div class="col-md-10">
                                                                        <textarea cols="5" rows="3" class="form-control" id="1" placeholder="Your Interestings" ></textarea>
                                                                    </div>-->
                                </form>
                            </div>
                        </div>

                        <hr>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="tabbable" id="tabs-1">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#panel-1" data-toggle="tab">Top Rated</a>
                                        </li>
                                        <li>
                                            <a href="#panel-2" data-toggle="tab">Most Visited</a>
                                        </li>
                                        <li>
                                            <a href="#panel-3" data-toggle="tab">Suggests</a>
                                            <!--                                            <a href="#panel-3" data-toggle="tab">Suggest by Month</a>-->
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="panel-1">
                                            <p>
                                                <!--Top Rated-->
                                            </p>
                                            <!--get places--> 
                                            <%
                                                Collections.sort(placeList, new PlaceCompRank());

                                                for (Places pls : placeList) {
                                                    PlacesImages plsImg = pm.getPlacesImage(pls);
                                                    Images img = null;
                                                    if (plsImg != null) {
                                                        img = plsImg.getImages();
                                                    } else {
                                                        img = new Images("Default", "/adventure/ella.jpg", 0, null);
                                                    }

                                            %>
                                            <div class="col-md-4">
                                                <div class="thumbnail" style="height: 303px">
                                                    <img alt="Bootstrap Thumbnail First" src="http://res.cloudinary.com/chaarika/image/upload/v1503077697/images/600X200<%=img.getImgUrl()%>" />
                                                    <div class="caption">
                                                        <h3>
                                                            <%=pls.getName()%>
                                                        </h3>
                                                        <p>
                                                            <%--<%=pls.getDescription()%>--%>
                                                            Rank : <%=pls.getRank()%>
                                                        </p>
                                                        <p>
                                                            <a class="btn btn-primary" href="#modal-container-<%=pls.getPlaceId()%>" data-toggle="modal">View</a> 
                                                            <!--<a class="btn btn-primary" href="#modal-container-460140" data-toggle="modal">View</a>--> 
                                                            <a class="btn btn-success" href="/Chaarika1/addToTour?placeId=<%=pls.getPlaceId()%>&interesting=<%=interesting%>">Add</a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <%
                                                }
                                            %>

                                        </div>
                                        <div class="tab-pane" id="panel-2">
                                            <p>
                                                <!--Most Visited-->
                                            </p>

                                            <!--get places--> 
                                            <%
                                                Collections.sort(placeList, new PlaceCompVisited());

                                                for (Places pls : placeList) {
                                                    PlacesImages plsImg = pm.getPlacesImage(pls);
                                                    Images img = null;
                                                    if (plsImg != null) {
                                                        img = plsImg.getImages();
                                                    } else {
                                                        img = new Images("Default", "/adventure/ella.jpg", 0, null);
                                                    }
                                            %>
                                            <div class="col-md-4">
                                                <div class="thumbnail" style="height: 303px">
                                                    <img alt="Bootstrap Thumbnail First" src="http://res.cloudinary.com/chaarika/image/upload/v1503077697/images/600X200<%=img.getImgUrl()%>" />
                                                    <div class="caption">
                                                        <h3>
                                                            <%=pls.getName()%>
                                                        </h3>
                                                        <p>
                                                            <%--<%=pls.getDescription()%>--%>
                                                            Rank : <%=pls.getRank()%>
                                                        </p>
                                                        <p>
                                                            <a class="btn btn-primary" href="#modal-container-<%=pls.getPlaceId()%>" data-toggle="modal">View</a> 
                                                            <!--<a class="btn btn-primary" href="#modal-container-460140" data-toggle="modal">View</a>--> 
                                                            <a class="btn btn-success" href="/Chaarika1/addToTour?placeId=<%=pls.getPlaceId()%>&interesting=<%=interesting%>">Add</a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <%
                                                }
                                            %>
                                        </div>
                                        <div class="tab-pane" id="panel-3">
                                            <p>
                                                <!--Month Suggests-->
                                            </p>

                                            <!--get places--> 
                                            <%
                                                Collections.sort(placeList, new PlaceCompVisited());

                                                for (Places pls : placeList) {
                                                    PlacesImages plsImg = pm.getPlacesImage(pls);
                                                    Images img = null;
                                                    if (plsImg != null) {
                                                        img = plsImg.getImages();
                                                    } else {
                                                        img = new Images("Default", "/adventure/ella.jpg", 0, null);
                                                    }
                                            %>
                                            <div class="col-md-4">
                                                <div class="thumbnail" style="height: 303px">
                                                    <img alt="Bootstrap Thumbnail First" src="http://res.cloudinary.com/chaarika/image/upload/v1503077697/images/600X200<%=img.getImgUrl()%>" />
                                                    <div class="caption">
                                                        <h3>
                                                            <%=pls.getName()%>
                                                        </h3>
                                                        <p>
                                                            <%--<%=pls.getDescription()%>--%>
                                                            Rank : <%=pls.getRank()%>
                                                        </p>
                                                        <p>
                                                            <a class="btn btn-primary" href="#modal-container-<%=pls.getPlaceId()%>" data-toggle="modal">View</a> 
                                                            <!--<a class="btn btn-primary" href="#modal-container-460140" data-toggle="modal">View</a>--> 
                                                            <a class="btn btn-success" href="/Chaarika1/addToTour?placeId=<%=pls.getPlaceId()%>&interesting=<%=interesting%>">Add</a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <%
                                                }
                                            %>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--                        <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="thumbnail">
                                                            <img alt="Bootstrap Thumbnail First" src="http://lorempixel.com/output/people-q-c-600-200-1.jpg" />
                                                            <div class="caption">
                                                                <h3>
                                                                    Kitulgala
                                                                </h3>
                                                                <p>
                                                                    Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
                                                                </p>
                                                                <p>
                                                                    <a class="btn btn-primary" href="#modal-container-460140" data-toggle="modal">View</a> 
                                                                    <a class="btn btn-success" href="#">Add</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="thumbnail">
                                                            <img alt="Bootstrap Thumbnail Second" src="http://lorempixel.com/output/city-q-c-600-200-1.jpg" />
                                                            <div class="caption">
                                                                <h3>
                                                                    Sri Padaya
                                                                </h3>
                                                                <p>
                                                                    Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
                                                                </p>
                                                                <p>
                                                                    <a class="btn btn-primary" href="#">View</a> <a class="btn btn-success" href="#">Add</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="thumbnail">
                                                            <img alt="Bootstrap Thumbnail Third" src="http://lorempixel.com/output/sports-q-c-600-200-1.jpg" />
                                                            <div class="caption">
                                                                <h3>
                                                                    Gall Fort
                                                                </h3>
                                                                <p>
                                                                    Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
                                                                </p>
                                                                <p>
                                                                    <a class="btn btn-primary" href="#">View</a> <a class="btn btn-success" href="#">Add</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>-->

                    </div>
                </div>


                <footer class="footer">
                    <div class="container-fluid">
                        <nav class="pull-left">
                            <ul>
                                <li>
                                    <a href="#">
                                        Home
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Company
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Portfolio
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Blog
                                    </a>
                                </li>
                            </ul>
                        </nav>
                        <p class="copyright pull-right">
                            &copy; <script>document.write(new Date().getFullYear())</script> <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
                        </p>
                    </div>
                </footer>

            </div>

            <!--Model Code-->
            <%
                for (Places pls : placeList) {
                    List<PlacesImages> plsImgList = pm.getPlacesImageLst(pls);
            %>
            <div class="col-md-12">
                <!--This is Model that show Detail View of Places-->
                <!--<a id="modal-460140" href="#modal-container-460140" role="button" class="btn btn-primary" data-toggle="modal">View</a>-->

                <div class="modal fade" id="modal-container-<%=pls.getPlaceId()%>" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <!--<div class="modal fade" id="modal-container-460140" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">-->
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">

                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                    ×
                                </button>
                                <h4 class="modal-title" id="myModalLabel">
                                    <%=pls.getName()%>
                                </h4>
                            </div>
                            <div class="modal-body">
                                <div class="col-md-12">
                                    <div class="carousel slide" id="carousel-638545">
                                        <ol class="carousel-indicators">
                                            <li data-slide-to="0" data-target="#carousel-638545"></li>
                                            <li data-slide-to="1" data-target="#carousel-638545"></li>
                                            <li data-slide-to="2" data-target="#carousel-638545" class="active"></li>
                                        </ol>
                                        <div class="carousel-inner">
                                            <%
                                                if (placeList != null && plsImgList!=null) {
                                                    for (int i = 0; i < plsImgList.size(); i++) {
                                                        PlacesImages plsImg = (PlacesImages) plsImgList.get(i);
                                                        Images img = null;
                                                        img = plsImg.getImages();
                                                        if (i == 0) {
                                            %>
                                            <div class="item active">
                                                <img alt="Bootstrap Thumbnail First" src="http://res.cloudinary.com/chaarika/image/upload/v1503077697/images/1600X500<%=img.getImgUrl()%>" />
                                                <div class="carousel-caption">
                                                </div>
                                            </div>
                                            <%
                                            } else {
                                            %>
                                            <div class="item">
                                                <img alt="Bootstrap Thumbnail First" src="http://res.cloudinary.com/chaarika/image/upload/v1503077697/images/1600X500<%=img.getImgUrl()%>" />
                                                <div class="carousel-caption">
                                                </div>
                                            </div>
                                            <%
                                                }
                                            %>

                                            <%
                                                }
                                            } else {
                                            %>
                                            <div class="item active">
                                                <img alt="Bootstrap Thumbnail First" src="http://res.cloudinary.com/chaarika/image/upload/v1503078625/images/1600X500/adventure/ella.jpg" />
                                                <div class="carousel-caption">
                                                </div>
                                            </div>
                                            <%
                                                }
                                            %>
                                        </div> <a class="left carousel-control" href="#carousel-638545" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#carousel-638545" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                                    </div>
                                    <h2>
                                        <%=pls.getName()%>
                                    </h2>
                                    <p>
                                        <%=pls.getDescription()%>
                                    </p>
                                    <p>
                                        <a class="btn" href="/Chaarika1/place.jsp?placeId=<%=pls.getPlaceId()%>" target="_blank">View details »</a> 
                                    </p>
                                </div>
                            </div>
                            <div class="modal-footer">

                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                    Close
                                </button> 
                                <!--                                            <button type="button" class="btn btn-primary">
                                                                                Save changes
                                                                            </button>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%
                }
            %>
            <!--Model Code END-->

            <!--body contend END-->

        </div>

    </body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.min.js"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Charts Plugin -->
    <!--<script src="assets/js/chartist.min.js"></script>-->

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>-->

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="assets/js/light-bootstrap-dashboard.js"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <!--<script src="assets/js/demo.js"></script>-->

    <script type="text/javascript">
                                $(document).ready(function () {

                                    demo.initChartist();

                                    $.notify({
                                        icon: 'pe-7s-gift',
                                        message: "Welcome to <b>Light Bootstrap Dashboard</b> - a beautiful freebie for every web developer."

                                    }, {
                                        type: 'info',
                                        timer: 4000
                                    });

                                });
    </script>

</html>
