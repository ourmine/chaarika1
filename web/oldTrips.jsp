<%-- 
    Document   : oldTrips
    Created on : Aug 18, 2017, 9:29:16 PM
    Author     : milinda
--%>
<%@page import="database.src.Images"%>
<%@page import="database.src.PlacesImages"%>
<%@page import="model.src.PlaceModel"%>
<%@page import="java.util.LinkedList"%>
<%@page import="java.util.List"%>
<%@page import="model.commons.comparators.SelectedPlsPriority"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Set"%>
<%@page import="database.src.SelectedPlaces"%>
<%@page import="database.src.Tours"%>
<%@page import="model.src.TourModel"%>
<%@page import="database.src.Users"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="assets/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

        <title>Visited Trips</title>


        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />


        <!-- Bootstrap core CSS     -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

        <!-- Animation library for notifications   -->
        <link href="assets/css/animate.min.css" rel="stylesheet"/>

        <!--  Light Bootstrap Table core CSS    -->
        <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


        <!--  CSS for Demo Purpose, don't include it in your project     -->
        <link href="assets/css/demo.css" rel="stylesheet" />


        <!--     Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

    </head>
    <body>

        <%
            Users user = (Users) session.getAttribute("user");
            if (user == null) {
                response.sendRedirect("/Chaarika1/login.jsp");
                return;
            }

            Integer selectedTourId = 0;

            if (request.getParameter("tourId") != null) {
                selectedTourId = Integer.parseInt(request.getParameter("tourId"));
            }

            TourModel tm = new TourModel();

            PlaceModel pm = new PlaceModel();

            List<Tours> tourList = tm.getFinishedTours(user);

            Tours tour = tm.getTour(selectedTourId);

            if (tour == null) {
        %>


        <div class="wrapper">
            <div class="sidebar" data-color="green" data-image="assets/img/sidebar-5.jpg">

                <!--   you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple" -->

                <!-- Side Bar -->
                <div class="sidebar-wrapper">
                    <div class="logo">
                        <a href="http://www.chaarikacom" class="simple-text">
                            Travel Partner
                        </a>
                    </div>

                    <ul class="nav">
                        <li>
                            <a href="dashboard.jsp">
                                <i class="pe-7s-graph"></i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li class="active">
                            <a href="profile.jsp">
                                <i class="pe-7s-user"></i>
                                <p>My Profile</p>
                            </a>
                        </li>

                        <li class="active-pro">
                            <a href="upgrade.html">
                                <i class="pe-7s-rocket"></i>
                                <p>Mobile Application</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="main-panel">
                <nav class="navbar navbar-default navbar-fixed">
                    <div class="container-fluid">
                        <div class="navbar-header">                            
                            <a class="navbar-brand" href="#">User | Finished Tour </a>
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a href="/Chaarika1/logout">
                                        <p>Log out</p>
                                    </a>
                                </li>
                                <li class="separator hidden-lg hidden-md"></li>
                            </ul>
                        </div>
                    </div>
                </nav>


                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <form action="/Chaarika1/oldTrips.jsp" method="get">
                                    <div class="form-group col-sm-8">
                                        <select name="tourId" class="form-control" >
                                            <option value="0" >--Selecet One--</option>
                                            <%                                                for (Tours oldTour : tourList) {
                                            %>
                                            <option value="<%=oldTour.getTourId()%>"><%=oldTour.getTourName()%></option>
                                            <%
                                                }
                                            %>

                                        </select>
                                    </div>
                                    <div class="form-group col-sm-2">
                                        <input type="submit" class="btn btn-success btn-block" value="View Tour">                                        
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <footer class="footer">
                    <div class="container-fluid">
                        <nav class="pull-left">
                            <ul>
                                <li>
                                    <a href="#">
                                        Home
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Company
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Portfolio
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Blog
                                    </a>
                                </li>
                            </ul>
                        </nav>
                        <p class="copyright pull-right">
                            &copy; <script>document.write(new Date().getFullYear())</script> <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
                        </p>
                    </div>
                </footer>

            </div>
        </div>


        <%
        } else {
            Set<SelectedPlaces> seltplses = tour.getSelectedPlaceses();

            List<SelectedPlaces> seltPlsList = new LinkedList<SelectedPlaces>(seltplses);
            Collections.sort(seltPlsList, new SelectedPlsPriority());

        %>


        <div class="wrapper">
            <div class="sidebar" data-color="green" data-image="assets/img/sidebar-5.jpg">

                <!--   you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple" -->

                <!-- Side Bar -->
                <div class="sidebar-wrapper">
                    <div class="logo">
                        <a href="http://www.chaarikacom" class="simple-text">
                            Travel Partner
                        </a>
                    </div>

                    <ul class="nav">
                        <li>
                            <a href="dashboard.jsp">
                                <i class="pe-7s-graph"></i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li class="active">
                            <a href="profile.jsp">
                                <i class="pe-7s-user"></i>
                                <p>My Profile</p>
                            </a>
                        </li>

                        <li class="active-pro">
                            <a href="upgrade.html">
                                <i class="pe-7s-rocket"></i>
                                <p>Mobile Application</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="main-panel">
                <nav class="navbar navbar-default navbar-fixed">
                    <div class="container-fluid">
                        <div class="navbar-header">                            
                            <a class="navbar-brand" href="#">User | Finished Tour </a>
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a href="/Chaarika1/logout">
                                        <p>Log out</p>
                                    </a>
                                </li>
                                <li class="separator hidden-lg hidden-md"></li>
                            </ul>
                        </div>
                    </div>
                </nav>


                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <form action="/Chaarika1/oldTrips.jsp" method="get">
                                    <div class="form-group col-sm-8">
                                        <select name="tourId" class="form-control" >
                                            <option value="0" >--Selecet One--</option>
                                            <%                                                for (Tours oldTour : tourList) {
                                            %>
                                            <option value="<%=oldTour.getTourId()%>"><%=oldTour.getTourName()%></option>
                                            <%
                                                }
                                            %>

                                        </select>
                                    </div>
                                    <div class="form-group col-sm-2">
                                        <input type="submit" class="btn btn-success btn-block" value="View Tour">                                        
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="content">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="card">
                                        <div class="header">
                                            <h4 class="title">Trip Map - <%=tour.getTourName()%></h4>                                         
                                        </div>

                                        <div class="content" style="height: 800px">

                                            <div class="col-md-12" style="height: 750px" id="map"></div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card card-user">
                                        <div class="header">
                                            <h4 class="title">Trip Places</h4>
                                        </div>
                                        <hr/>
                                        <div class="content">                                        
                                            <%
                                                for (SelectedPlaces plas : seltPlsList) {
                                            %>
                                            <a class="btn btn-block" href="#modal-container-<%=plas.getPlaces().getPlaceId()%>" data-toggle="modal"><%=plas.getPlaces().getName()%></a>                                        
                                            <%
                                                }
                                            %>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                    <footer class="footer">
                        <div class="container-fluid">
                            <nav class="pull-left">
                                <ul>
                                    <li>
                                        <a href="#">
                                            Home
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Company
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Portfolio
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Blog
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                            <p class="copyright pull-right">
                                &copy; <script>document.write(new Date().getFullYear())</script> <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
                            </p>
                        </div>
                    </footer>

                </div>
            </div>


            <!--Model Code-->
            <div class="col-md-12">
                <!--This is Model that show Detail View of Places-->
                <!--<a id="modal-460140" href="#modal-container-460140" role="button" class="btn btn-primary" data-toggle="modal">View</a>-->
                <%
                    for (SelectedPlaces plas : seltPlsList) {
                        List<PlacesImages> plsImgList = pm.getPlacesImageLst(plas.getPlaces());

                %>
                <div class="modal fade" id="modal-container-<%=plas.getPlaces().getPlaceId()%>" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">

                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                    ×
                                </button>
                                <h4 class="modal-title" id="myModalLabel">
                                    <%=plas.getPlaces().getName()%>
                                </h4>
                            </div>
                            <div class="modal-body">
                                <div class="col-md-12">
                                    <div class="carousel slide" id="carousel-638545">
                                        <ol class="carousel-indicators">
                                            <li data-slide-to="0" data-target="#carousel-638545"></li>
                                            <li data-slide-to="1" data-target="#carousel-638545"></li>
                                            <li data-slide-to="2" data-target="#carousel-638545" class="active"></li>
                                        </ol>
                                        <div class="carousel-inner">
                                            <%
                                                if (plsImgList != null) {
                                                    for (int i = 0; i < plsImgList.size(); i++) {
                                                        PlacesImages plsImg = (PlacesImages) plsImgList.get(i);
                                                        Images img = null;
                                                        img = plsImg.getImages();
                                                        if (i == 0) {
                                            %>
                                            <div class="item active">
                                                <img alt="Bootstrap Thumbnail First" src="http://res.cloudinary.com/chaarika/image/upload/v1503077697/images/1600X500<%=img.getImgUrl()%>" />
                                                <div class="carousel-caption">
                                                </div>
                                            </div>
                                            <%
                                            } else {
                                            %>
                                            <div class="item">
                                                <img alt="Bootstrap Thumbnail First" src="http://res.cloudinary.com/chaarika/image/upload/v1503077697/images/1600X500<%=img.getImgUrl()%>" />
                                                <div class="carousel-caption">
                                                </div>
                                            </div>
                                            <%
                                                }
                                            %>

                                            <%
                                                }
                                            } else {
                                            %>
                                            <div class="item active">
                                                <img alt="Bootstrap Thumbnail First" src="http://res.cloudinary.com/chaarika/image/upload/v1503078625/images/1600X500/adventure/ella.jpg" />
                                                <div class="carousel-caption">
                                                </div>
                                            </div>
                                            <%
                                                }
                                            %>
                                        </div> <a class="left carousel-control" href="#carousel-638545" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#carousel-638545" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                                    </div>
                                    <h2>
                                        <%=plas.getPlaces().getName()%>
                                    </h2>
                                    <p>
                                        <%=plas.getPlaces().getDescription()%>
                                    </p>
                                    <p>
                                        <a class="btn" href="/Chaarika1/place.jsp?placeId=<%=plas.getPlaces().getPlaceId()%>" target="_blank">View details »</a> 
                                    </p>
                                </div>
                            </div>
                            <div class="modal-footer">

                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                    Close
                                </button> 
                                <!--                                            <button type="button" class="btn btn-primary">
                                                                                Save changes
                                                                            </button>-->
                            </div>
                        </div>
                    </div>
                </div>
                <%
                    }
                %>
            </div>
            <!--Model Code END-->


            <script>
                function initMap() {
                    var locations = [
                <%
                    for (SelectedPlaces plas : seltPlsList) {
                %>
                        ['<%=plas.getPlaces().getName()%>', <%=plas.getPlaces().getLatitude()%>, <%=plas.getPlaces().getLongitude()%>,<%=plas.getPriority()%>],
                <%
                    }
                %>
                    ];
                    var map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 8,
                        center: new google.maps.LatLng(7.8731, 80.7718),
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    });


                    var path = new google.maps.MVCArray();
                    var service = new google.maps.DirectionsService();
                    var poly = new google.maps.Polyline({
                        map: map,
                        strokeColor: '#F3443C'
                    });

                    path.push(new google.maps.LatLng(7.1802, 79.8843));

                    var infowindow = new google.maps.InfoWindow();
                    var marker, i;
                    for (i = 0; i < locations.length; i++) {
                        marker = new google.maps.Marker({
                            icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
                            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                            map: map,
                            description: locations[i][3],
                            strokeColor: '#F3443C'
                        });
                        google.maps.event.addListener(marker, 'click', (function (marker, i) {
                            return function () {
                                infowindow.setContent(locations[i][0]);
                                infowindow.open(map, marker);
                            }
                        })(marker, i));
                    }
                    for (var i = 0; i < locations.length; i += 2) {
                        if ((i + 1) < locations.length) {
                            var src = new google.maps.LatLng(locations[i][1], locations[i][2]);
                            var des = new google.maps.LatLng(locations[i + 1][1], locations[i + 1][2]);

                            poly.setPath(path);
                            service.route({
                                origin: src,
                                destination: des,
                                travelMode: google.maps.DirectionsTravelMode.DRIVING
                            }, function (result, status) {
                                if (status == google.maps.DirectionsStatus.OK) {
                                    for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                                        path.push(result.routes[0].overview_path[i]);
                                    }
                                }
                            });
                        }
                    }
                }
            </script>
            <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBaBKK9V4AVKsfZ0K5Yp2n_eky4HqleJrk&callback=initMap">
            </script>
        </div>

        <%            }


        %>



    </body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.min.js"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Charts Plugin -->
    <!--<script src="assets/js/chartist.min.js"></script>-->

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="assets/js/light-bootstrap-dashboard.js"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <!--<script src="assets/js/demo.js"></script>-->

</html>